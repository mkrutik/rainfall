#include <stdio.h>
#include <unistd.h>
#include <string.h>

void p(char * dest, char * s)
{
    char var_1008h[0x1008];

    puts(s);
    read(0, var_1008h, 0x1000);
    // find new line
    *strchr(var_1008h, 0xa) = 0;
    strncpy(dest, var_1008h, 0x14);
}

void pp(char * dest)
{
    char *var_3ch;
    char var_30h[0x30];
    int var_1ch;
    char *src;

    p(var_30h, (char*)0x80486a0); // 0x80486a0 - " - " some random piece of memory
    p((char*)&var_1ch, (char*)0x80486a0); // 0x80486a0 - " - " some random piece of memory
    strcpy(dest, var_30h);
    dest[asm_strlen(dest)] = 0;
    strcat(dest, (char*)&var_1ch); // problem code
}

int main(void)
{
    char s[52]; // target
    pp(s);
    puts(s);
    return 0;
}
