// Note: compile with -fno-stack-protector
#include <stdio.h>
#include <stdlib.h>

void run(void)
{
    fwrite ("Good... Wait what?\n", stdout, 0x13, 1);
    system ("/bin/sh"); // to hold system function stdin - should be open
}

int main (void) {
    char s[76];
    gets(s);            // didn't expect buffer overflow
    return 0;
}