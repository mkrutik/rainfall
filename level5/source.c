#include <stdio.h>
#include <stdlib.h>

void o(void)
{
    system("/bin/sh");
    exit(1);
}

void n(void)
{
    char buff[0x200];
    int size = 0x200;
    FILE * stream = stdin;

    fgets(buff, size, stream);
    
    printf(buff); // target
    
    exit(1);
}

int main(void)
{
    n();
}