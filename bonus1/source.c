#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv)
{
    void    *s2;
    size_t  n;
    char    s1[40];
    int     var_8h;

    var_8h = atoi(argv[1]);
    if (var_8h > 9) // problem code
    {
        return 1;
    }
    else
    {
        memcpy(s1, argv[2], var_8h * 4); // target
        if (var_8h == 0x574f4c46)
        {
            n = 0;
            *((unsigned*)s2) = 0x8048580; // "sh"
            execl("/bin/sh", (char*)s2, n);
        }
    }
    return 0;
}