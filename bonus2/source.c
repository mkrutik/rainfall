#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int language = 0;

void greetuser(char *arg) {
    char s1[0x44];
    char s2[0x4];

    if (language == 1)
    {
        // asm copy instructions
        strcpy(s1, "Hyv\xc3\xa4\xc3\xa4 p\xc3\xa4iv\xc3\xa4\xc3\xa4 ");
    }
    else if (language == 2)
    {
        // asm copy instructions
        strcpy(s1, "Goedemiddag! ");
    }
    else if (language == 0)
    {
        // asm copy instructions
        strcpy(s1, "Hello ");
    }

    strcat(s1, arg); // problem code
    puts(s1);
}

int main(int argc, char **argv)
{
    char    src[0x4];
    size_t  n;
    char    dest[0x48];
    char    *s1;

    if (argc != 3)
    {
        return 1;
    }

    memset(dest, 0x13, 0);
    strncpy(dest, argv[1], 0x28);
    strncpy((dest + 0x28), argv[2], 0x20);

    s1 = getenv("LANG");
    if (s1 != 0)
    {
        n = memcmp(s1, (char*)0x804873d, 2); // 0x804873d "fi"
        if (n == 0)
        {
            language = 1;
        }
        else
        {
            n = memcmp (s1, (char*)0x8048740, 2); // 0x8048740 "nl"
            if (n == 0)
            {
                language = 2;
            }
        }
    }

    memcpy(src, dest, 0x13);
    greetuser(dest);
}