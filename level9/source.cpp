#include <string.h>
#include <stdlib.h>

class N
{
public:
    // here is vtable
    char arr[108];

public:
    N(int var)
    {
        *((int*)arr[104]) = var;
    }

    virtual N& operator+ (N& var)
    {
        *((int*)arr[104]) = *((int*)arr[104]) + *((int*)var.arr[104]);
        return *this;
    }

    N& operator- (N& var)
    {
        *((int*)arr[104]) = *((int*)arr[104]) - *((int*)var.arr[104]);
        return *this;
    }

    void setAnnotation(char* var)
    {
        memcpy(arr, var, strlen(var)); // target
    }
};

int main(int argc, char **argv)
{
    if (argc <= 1)
        exit(1);

    N *n = new N(5);
    N *n = new N(6);
    n->setAnnotation(argv[1]);
    n->operator+(*n);
}


/* C / ASM version

// N::operator+(N&)
int fcn.0804873a (int arg_8h, int arg_ch)
{
    eax = arg_8h;
    edx = *((eax + 0x68));
    eax = arg_ch;
    eax = *((eax + 0x68));
    eax += edx;
    return eax;
}

// N::N(int)
int method_N_N_int (int arg_8h, int arg_ch)
{
    eax = arg_8h;
    *(eax) = fcn.0804873a;
    eax = arg_8h;
    edx = arg_ch;
    *((eax + 0x68)) = edx;
    return eax;
}

// N::operator-(N&)
int method_N_operator_N (int arg_8h, int arg_ch)
{
    eax = arg_8h;
    edx = *((eax + 0x68));
    eax = arg_ch;
    eax = *((eax + 0x68));
    ecx = edx;
    ecx -= eax;
    eax = ecx;
    return eax;
}

// N::setAnnotation(char*)
int method_N_setAnnotation_char (void * s1, char * s)
{
    char * s2;
    size_t n;
    eax = s;
    eax = strlen (eax);
    edx = s1;
    edx += 4;
    eax = s;
    memcpy (edx, eax, s); // target
    return eax;
}

int main(int arg_8h, int arg_ch)
{
    int var_4h;
    int var_24h;
    int var_18h;
    int var_14h;
    int var_10h;
    int var_ch;
    if (arg_8h <= 1) {
        exit (1);
    }
    eax = operatornew(unsignedint) (0x6c);
    ebx = eax;
    N::N(int) (ebx, 5);
    eax = operatornew(unsignedint) (0x6c, ebx);
    ebx = eax;
    N::N(int) (ebx, 6);
    var_10h = ebx;
    eax = var_ch;
    var_14h = var_ch;
    eax = var_10h;
    var_18h = var_10h;
    eax = arg_ch;
    eax += 4;
    eax = *(eax);
    eax = var_14h;
    N::setAnnotation(char*) (eax, *(eax));
    eax = var_18h;
    eax = *(eax); // interesting place
    edx = *(eax); // interesting place
    eax = var_14h;
    eax = var_18h;
    void (*edx)(void, void) (eax, var_14h);
    ebx = var_4h;
    return eax;
}
*/