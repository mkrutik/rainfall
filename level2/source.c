#include <stdio.h>

void p()
{
    char arr[76];

    fflush(stdout);
    gets(arr);

    if ((ebp & 0xb0000000) != 0xb0000000) // check that return address was overrided
    {
        printf("(%p)\n", ebp);
        exit (1);
    }
    puts(arr);
    char *str = strdup(arr);
}

int main()
{
    // our target return address here
    p();
}