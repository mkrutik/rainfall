#include <stdio.h>
#include <string.h>
#include <stdbool.h>

char *service;
char *auth;

int main(void)
{
    int     var_8h;
    char    *size;
    FILE    *nitems;
    FILE    *stream;
    int     var_90h;
    char    src[0x80];

    while (true)
    {
        printf("%p, %p \n", auth, service);

        stream = stdin;
        if (fgets(src, 0x80, stream) == 0)
        {
            return 0;
        }

        if (asm_strncpy(src, "auth ", 5) == 0)
        {
            *auth = malloc(4);
            *((int*)*auth) = 0;

            if (asm_strlen(src + 5) <= 0x1e) {
                strcpy(*auth, (src + 5));
            }
        }

        if (asm_strncpy(src, "reset", 5) == 0)
        {
            free((void*)*auth);
        }

        if (asm_strncpy(src, "service", 7) == 0) // target
        {
            *service = strdup(src + 7);
        }

        if (asm_strncpy(src, "login", 5) != 0)
        {
            continue;
        }

        if (*((int*)*auth + 0x20) != 0)
        {
            if (system ("/bin/sh") != 0)
                break;

            continue;
        }

        if (fwrite("Password:\n", 1, 0xa, stdout) != 0)
            break;
    }

    return 0;
}