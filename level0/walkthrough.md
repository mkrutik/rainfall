After logn we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX enabled    No PIE          No RPATH   No RUNPATH   /home/user/level0/level0
```

Command: `ls -l`
```
total 732
-rwsr-x---+ 1 level1 users 747441 Mar  6  2016 level0
```

I used `radare` reverse engineering tool to analyze binary.  
The main problem of this binary is that they didn't check the number of input arguments before they have used it.
> More info about `radar`: https://www.radare.org/r/index.html

This binary expects one argument - and it should be `423`.

Command: `./level0 423`  
After this shell be with `level1` permission will start.

Command: `cat /home/user/level1/.pass`  
Output: `1fe8a524fa4bec01ca4ea2a869af2a02260d4a7d5fe7e7c24d8617e6dca12d3a`

Flag: `1fe8a524fa4bec01ca4ea2a869af2a02260d4a7d5fe7e7c24d8617e6dca12d3a`