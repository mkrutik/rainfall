#define _GNU_SOURCE
#include <stdio.h>      // fwrite
#include <stdlib.h>     // atoi
#include <unistd.h>     // geteuid getegid
#include <sys/types.h>  // geteuid getegid
#include <string.h>     // strdup

int main(int argc, char *argv[])
{
    char *arr[2];
    gid_t g_id;
    uid_t u_id;

    if (423 == atoi(argv[1])) // they didn't check number of arguments
    {
        arr[0] = strdup("/bin/sh");
        arr[1] = NULL;

        g_id = getegid();
        u_id = geteuid();
        setresgid(g_id, g_id, g_id);
        setresuid(u_id, u_id, u_id);
        execv("/bin/sh", arr);
    }
    else 
    {
        fwrite("No !\n", 1, 5, stderr);
    }

    return 0;
}